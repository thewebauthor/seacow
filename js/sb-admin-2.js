$(function() {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent().parent().addClass('in').parent().addClass('active');
    if (element.is('li')) {
        element.addClass('active');
		element.parent().parent().addClass('active');
    }
});

$( document ).ready(function() {
    var url = window.location.href;
	
	url = url.split("?").pop();
	
	url = "index.php?" + url;
	
    $('ul.nav a[href$="'+ url +'"]').addClass('active').parent().parent().addClass('in').parent().addClass('active');

    // Will also work for relative and absolute hrefs
    $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent().parent().addClass('in').parent().addClass('active');
});

function prevent_unsaved_edits_loss(from_preview) {
	var formHasChanged = false;
	var submitted = false;

	$(document).on('change', 'form input, form select, form textarea', function (e) {
		formHasChanged = true;
	});

	$(document).ready(function () {
	window.onbeforeunload = function (e) {
		if ((formHasChanged && !submitted) || (from_preview && !submitted)) {
			var message = "You have not yet saved your changes.", e = e || window.event;
			if (e) {
				e.returnValue = message;
			}
			return message;
		}
	}
	$("form").submit(function() {
	 submitted = true;
	 });
	});
}