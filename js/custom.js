function notify (msg, delay, type){
	delay = delay * 1000;
	switch (type) {
		case 'Success':
			$(".notify").hide().html('<div class="alert alert-success">'+msg+'</div>').fadeIn('slow');;		
			$(".alert").delay(delay).fadeOut("slow");
		break;
		case 'Notice':
			$(".notify").hide().html('<div class="alert alert-warning">'+msg+'</div>').fadeIn('slow');;
			$(".alert").delay(delay).fadeOut("slow");
		break;
		case 'Error':
			$(".notify").hide().html('<div class="alert alert-danger">'+msg+'</div>').fadeIn('slow');;
			$(".alert").delay(delay).fadeOut("slow");
		break;
	}
}

// Nav icon fixes
$('ul.nav-second-level').each(function() {
	$(this).on('show.bs.collapse', function() {
		$(this).siblings('a').children('svg[data-icon=angle-left]').each(function(index, value) {
			$(value).removeClass('fa-angle-left').addClass('fa-angle-down');
		});
	}).on('hide.bs.collapse', function() {
		$(this).siblings('a').children('svg[data-icon=angle-down]').each(function(index, value) {
			$(value).removeClass('fa-angle-down').addClass('fa-angle-left');
		});
	});
});

$('ul.nav-third-level').each(function() {
	$(this).on('show.bs.collapse', function() {
		$(this).siblings('a').children('svg[data-icon=angle-left]').each(function(index, value) {
			$(value).removeClass('fa-angle-left').addClass('fa-angle-down');
		});
	}).on('hide.bs.collapse', function() {
		$(this).siblings('a').children('svg[data-icon=angle-down]').each(function(index, value) {
			$(value).removeClass('fa-angle-down').addClass('fa-angle-left');
		});
	});
});