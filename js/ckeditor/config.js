/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.toolbarGroups = [
		{ name: 'basicstyles', groups: [ 'basicstyles', 'styles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'blocks', 'align'] },
		{ name: 'links', groups: [ 'colors', 'links' ] }
	];
	};