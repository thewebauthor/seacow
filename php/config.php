<?php 
/*
*	Set variables and functions; first page to be included
*	Author: Daniel Harris @thewebauthor
*/

/*
*   Set protocol string to https or http
*/
$protocol = '';
if(!empty($_SERVER['HTTP_X_FORWARDED_PROTO'])){
    $protocol .= $_SERVER['HTTP_X_FORWARDED_PROTO'].'://';
}
else{
    $protocol .= !empty($_SERVER['HTTPS']) ? "https://" : "http://";
}

/*
*   Set URL variables
*/
$url = $_SERVER['REQUEST_URI'];
$parts = explode('/',$url);
$dir = $_SERVER['SERVER_NAME'];
for ($i = 0; $i < count($parts) - 1; $i++) {
 $dir .= $parts[$i] . "/";
}

$domain = $protocol.$dir;

$root_dir = getcwd();

// Database Credentials
require(__DIR__."/../creds.php");

// Connect and use database
connect($db_host, $db_user, $db_pw, $db_name); 

$admin_email = select_row("value", "`s_site-settings`", "slug='admin-email'", "");

/*
*   Get max columns to show in tables
*/
$max_columns = select_row("`max-columns`", "s_types", "slug='$type'", "");
$max_columns = $max_columns ? $max_columns : select_row("value", "`s_site-settings`", "slug='max-columns'", "");

/*
*   Set site title string
*/
$site_title = select_row("value", "`s_site-settings`", "slug='site-title'", "");

/*
*   Todo: use settings global variables
*/
$settings['admin_email'] = $admin_email;
$settings['max_columns'] = $max_columns;
$settings['domain'] = $domain;
$settings['root_dir'] = $root_dir;
$settings['site_title'] = $site_title;

/*
*   Set table string names
*/
$type_pl = $type ? plural($type) : '';
$type_s = $type ? select_row("singular", "s_types", "slug='$type'", "") : '';
$type_name = $type_name ? $type_name : select_row("name", "s_types", "slug='$type'", "");
?>
