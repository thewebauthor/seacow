<?php
/*
*	Handle database ajax requests from clicking and editing table cells
*	Author: Daniel Harris @thewebauthor
*/

foreach ($_GET as $key => $value) {
	$$key = $value;
}
foreach ($_POST as $key => $value) {
	$$key = $value;
}

require_once 'lib.php';
require_once 'config.php';

switch($action) {
	case "select":
		generate_select($col_id, '', $v, $class);
	break;
	case "edit":
		$val = '';
		$duplicate_col = false;
		switch($t){
			case "date":
				if (empty($v)) {
					$v = "NULL";
				}
				else {
					$v = format_date($v, 'Y-m-d');
				}
			break;
			case "datetime":
				if (empty($v)) {
					$v = "NULL";
				}
				else {
					$v = mysqli_date($v);
				}
			break;
			case "time":
				if (empty($v)) {
					$v = "NULL";
				}
				else {
					$v = format_date($v, 'H:i:s');
				}
			break;
		}
		
		$col = select_row("slug", "s_columns", "id='$col_id'", "");

		if ($table == 's_columns') {
			$the_col = select_row("name, slug, type, `length`, `table`", "s_columns", "id='$row_id'");
			$tabl = select_row("slug", "s_types", "id='{$the_col['table']}'", "");			
			
			switch($col) {
				case 'length':
					q("ALTER TABLE `$tabl` MODIFY `{$the_col['slug']}` ".to_mysql_col_type($the_col['type'], $v));
				break;
				case 'slug':
					if (!col_exists($v, $tabl)) {
						q("ALTER TABLE `$tabl` CHANGE `$old_v` `$v` ".to_mysql_col_type($the_col['type'], $the_col['length']));
					}
					else {
						$duplicate_col = true;
					}
				break;
				case 'type':
					q("ALTER TABLE `$tabl` MODIFY `{$the_col['slug']}` ".to_mysql_col_type($v, $the_col['length']));
				break;
			}
		}		
		
		if ($v === "NULL") {
			q("UPDATE `$table` SET `$col`=NULL WHERE id='$row_id'");
		}
		else {
			if (!$duplicate_col) {
				q("UPDATE `$table` SET `$col`='$v' WHERE id='$row_id'");
			}
		}
		
		if ($table == 's_types') {
			if ($col == 'slug') {
				$v = format_type(toslug($v));
				q("RENAME TABLE `$old_v` TO `$v`");
			}
		}
		
		switch($t) {
			case "date":
				if ($v === "NULL") {
					$v = "";
				}
				else {
					$v = format_date($v, 'F j, Y');
				}
			break;
			case "datetime":
				if ($v === "NULL") {
					$v = "";
				}
				else {
					$v = format_date($v, 'F j, Y g:i a');
				}
			break;
			case "time":
				if ($v === "NULL") {
					$v = "";
				}
				else {
					$v = format_date($v, 'g:i a');
				}
			break;
			case "select":
				$select = select_row("`text`, from_table", "s_selects", "col_id='$col_id'", "");
				$from_table = select_row("slug", "s_types", "id='{$select['from_table']}'", "");
				$v_id = $v;
				$v = select_row('`'.$select['text'].'`', '`'.$from_table.'`', "id='$v_id'", "");
				$val = $v;
				$v .= "\n<input type='hidden' value='$v_id'>";
			break;
		}
		
		if ($duplicate_col) {
			echo $old_v;
		}
		else {
			switch($t) {
				//$v contains <input type='hidden'> element while $val doesn't for select
				case 'select':
					if ($val == '') {
						echo 'N/A';
					}
					else {
						echo $v;
					}
				break;
				default:
					if ($v == '') {
						echo 'N/A';
					}
					else {
						echo $v;
					}
				break;
			}
		}
	break;
	case "delete":
		if ($type == 's_types') {
			$table = select_row("slug", "s_types", "id='$row_id'", "");
		}
		if ($type == 's_columns') {
			$table_id = select_row("`table`", "s_columns", "id='$row_id'", "");
			$table = select_row("slug", "s_types", "id='$table_id'", "");
			$column = select_row("slug", "s_columns", "id='$row_id'", "");
		}
		if (q("DELETE FROM `$type` WHERE id='$row_id'")) {
			if ($type == 's_types') {
				q("DROP TABLE `$table`");
				q("DELETE FROM s_selects WHERE col_id IN (SELECT id FROM s_columns WHERE `table`='$row_id')");
				q("DELETE FROM s_columns WHERE id = (SELECT col_id FROM s_selects WHERE from_table='$row_id')");
				q("DELETE FROM s_columns WHERE `table`='$row_id'");
			}
			if ($type == 's_columns') {
				q("DELETE FROM s_selects WHERE col_id='$row_id'");
				q("ALTER TABLE `$table` DROP COLUMN `$column`");
			}
		}
	break;
}
?>