<?php
/*
*	Top and side navigation bars
*	Author: Daniel Harris @thewebauthor
*/
?>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=$domain?>"><?=$site_title?></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
				<!--
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    
                </li>
				-->

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="?action=edit&type=s_users&id=<?=$_SESSION['user']['id']?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="?action=list&type=s_site-settings"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="?action=logout""><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!--
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                        </li>
                        -->
                        <li>
                            <a href="<?=$domain?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li><br></li>
                        <?php
                            /*
                            *	Get user tables and print into side navigation bar
                            */				
                            $types = select("name, slug, icon", "s_types", "reserved='No' AND hide='No' AND category=''", "ORDER BY `order` ASC");
                            foreach ($types as $t) {
                                echo '
                                    <li>
                                        <a href="?action=list&type='.$t['slug'].'">'.($t['icon'] ? '<i class="fa fa-'.$t['icon'].' fa-fw"></i> ' : '').ucwords($t['name']).'</a>
                                    </li>
                                    ';
                            }								
							?>
						<li>
                            <?php
                            /*
                            *	Get user table categories and print into side navigation bar
                            */	                            				
							$categories = select("id, name, icon", "`s_type-categories`", "", "ORDER BY `order` ASC");
							
							foreach ($categories as $category) {
								echo '<li>';
									echo '<a href="#">'.($category['icon'] ? '<i class="fa fa-'.$category['icon'].' fa-fw"></i> ' : '').ucwords($category['name']).'<i class="fas fa-angle-left arrow"></i></a>';
									echo '<ul class="nav nav-second-level">';
									
									$types = select("name, slug, icon", "s_types", "reserved='No' AND hide='No' AND category='{$category['id']}'", "ORDER BY `order` ASC");
									foreach ($types as $t) {
										echo '
											<li>
												<a href="?action=list&type='.$t['slug'].'">'.($t['icon'] ? '<i class="fa fa-'.$t['icon'].' fa-fw"></i> ' : '').ucwords($t['name']).'</a>
											</li>
											';
									}								
									echo '</ul>';
								echo '</li>';
							}
							?>
						</li>
						<!--<li>
							<a href="#"><i class="fa fa-bar-chart fa-fw"></i> Reports<i class="fas fa-angle-left arrow"></i></a>
							<ul class="nav nav-second-level">
							
							</ul>
						</li>-->
						<li>
							<a href="#"><i class="fa fa-cog fa-fw"></i> Settings<i class="fas fa-angle-left arrow"></i></a>
							<ul class="nav nav-second-level">
								<?php
                                    /*
                                    *	Get system tables and print into side navigation bar
                                    */	
									$tables = select("name, slug, singular, icon", "s_types", "reserved='Yes'", "ORDER BY `order` ASC");
									foreach ($tables as $t) {
										echo '
										<li>
											<a href="#">'.($t['icon'] ? '<i class="fa fa-'.$t['icon'].' fa-fw"></i> ' : '').ucwords($t['name']).'<i class="fas fa-angle-left arrow"></i></a>
											<ul class="nav nav-third-level">
												<li>
													<a href="?action=list&type='.$t['slug'].'">List '.ucwords($t['name']).'</a>
												</li>
												<li>
													<a href="?action=add&type='.$t['slug'].'">Add '.ucwords($t['singular']).'</a>
												</li>
											</ul>
										</li>';
									}
								?>
							</ul>
						</li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>