<?php
/*
*	Page template for the main dashboard page.
*	Author: Daniel Harris @thewebauthor
*/
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?=$site_title?></title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Font Awesome -->
    <!--<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js"></script>
  	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/v4-shims.js"></script>
	
	<!-- Custom CSS -->
	<link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
		<?php require_once 'templates/nav.php'; ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">            
                <?php
                    $types = select("slug, name, singular, icon", "s_types", "reserved='No'", "");
                    
                    foreach($types as $type) {
                        echo "<div class='col-lg-4 col-12'>";
                            echo "<h2>".($type['icon'] ? '<i class="fa fa-'.$type['icon'].' fa-fw"></i> ' : '').$type['name']."</h2>";
                            echo "<ul class='dash-list'>";
                                echo "<li><a href='?action=list&type={$type['slug']}'>View {$type['name']}</a></li>";
                                echo "<li><a href='?action=add&type={$type['slug']}'>Add {$type['singular']}</a></li>";
                            echo "</ul>";
                        echo "</div>";
                    }
                ?>
            </div>            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <!--<script src="vendor/raphael/raphael-min.js"></script>
    <script src="vendor/morrisjs/morris.min.js"></script>
    <script src="js/morris-data.js"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="js/sb-admin-2.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>
