<?php
/*
*	Login page template
*	Author: Daniel Harris @thewebauthor
*/

switch($action) {
	case "forgot_pw":
		$token = md5(uniqid(rand(), true));
		$match = select_row("id", "s_users", "email='$email'", "");
		if ($match) {
			$q = "UPDATE s_users SET token='$token' WHERE email='$email'";
			if (q($q)) {
				if (mail_reset_pw($token, $email)) {
					notify("We sent you an email containing the link to reset your password.", 5, "Success");
				}
			}				
		}
		else {
			notify("We couldn't find your email in the database. Please try again.", 5, "Notice");
		}
	break;
	case "reset_form":
		$match = select_row("id", "s_users", "email='$email' AND token='$token'", "");
		if ($match) {
			$reset_form = true;
		}
	break;
	case "reset_pw":
		if ($password && $cpassword) {
			$password = md5(escape($password).'25');
			$q = "UPDATE s_users SET password='$password', token='' WHERE email='$email' AND token='$token'";
			if (q($q)) {
				notify("Your password has been successfully reseted!", 5, "Success");
			}
		}
	break;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" href="favicon.ico">

    <title>Login to <?=$site_title?></title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<script src="vendor/jquery/jquery.min.js"></script>
	
	<script src="js/custom.js"></script>
  </head>
  
  <body>

    <div class="container">

      <form action="?action=login" method="post" class="form-signin">
		<div id="logo">
			<img src="images/logo.png">
		</div>
        <h2 class="form-signin-heading">Please sign in</h2>
		<div class="notify"></div>
		<?php notify_write(); ?>
        <label for="inputText" class="sr-only">Username</label>
        <input type="text" name="username" id="inputText" class="form-control" placeholder="Username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->
	
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>