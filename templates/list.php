<?php
/*
*	List page template for tables
*	Author: Daniel Harris @thewebauthor
*/

$data = $_SESSION['data'];
$the_cols = $_SESSION['the_cols'];

/*
*	Parse and insert edited data after submission from an edit page
*/
if ($data && $edit) {
	
	if ($type == 's_users' && $data['password']){
		$data['password'] = md5($data['password'].'2015');
	}
	if ($type == 's_columns') {
		$data['slug'] = toslug($data['slug']);
		$table = select_row("slug", "s_types", "id='{$data['table']}'", "");
	}
	if ($type == 's_types') {
		$data['slug'] = format_type(toslug($data['slug']));
	}
	
	$keys = array_keys($data);
	$values = array_values($data);	
	
	$sql = "UPDATE `$type` SET";
	
	for($x=0; $x < count($keys); $x++) {
		switch($the_cols[$keys[$x]]['type']) {
			case 'date':
				if (empty($values[$x])) {
					$values[$x] = "NULL";
				}
				else {
					$values[$x] = date('Y-m-d', strtotime($values[$x]));
				}
			break;
			case 'datetime':
				if (empty($values[$x])) {
					$values[$x] = "NULL";
				}
				else {
					$values[$x] = date('Y-m-d H:i:00', strtotime($values[$x]));
				}
			break;
			case 'time':
				if (empty($values[$x])) {
					$values[$x] = "NULL";
				}
				else {
					$values[$x] = date('H:i:00', strtotime($values[$x]));
				}
			break;
		}
		
		if ($x === 0) {
			$sql .= ' `'.$keys[$x].'`="'.escape($values[$x]).'"';
		}
		else {
			$sql .= ', `'.$keys[$x].'`="'.escape($values[$x]).'"';
		}
	}
	$sql = str_replace('"NULL"', "NULL", $sql);
	
	$sql .= " WHERE id='$edit'";
	
	if (q($sql)) {
		if ($type == 's_types') {
			$new_slug = $data['slug'];
			
			if (q("UPDATE s_columns SET `table`='$new_slug' WHERE `table`='$old_slug'")) {
				if ($old_slug != $new_slug) {
					q("RENAME TABLE `$old_slug` TO `$new_slug`");
				}
			}
		}
		if ($type == 's_columns') {
			$new_col_name = toslug($data['slug']);
			$table = select_row("slug", "s_types", "id='{$data['table']}'", "");
			
			q("ALTER TABLE `$table` CHANGE `$old_col_name` `$new_col_name` ".to_mysql_col_type($data['type'], $data['length']));
		}
	}
}

/*
*	Parse and insert new data after submission from an add page
*/
if ($data && $add) {
	$duplicate_col = false;
	
	if ($type == 's_users' && $data['password']){
		$data['password'] = md5($data['password'].'2015');
	}
	if ($type == 's_columns') {
		$data['slug'] = toslug($data['slug']);
		$table = select_row("slug", "s_types", "id='{$data['table']}'", "");
		$duplicate_col = col_exists($data['slug'], $table);
	}
	if ($type == 's_types') {
		$data['slug'] = format_type(toslug($data['slug']));
	}
	
	$keys = array_keys($data);
	$values = array_values($data);
	
	for($x=0; $x < count($keys); $x++) {
		switch($the_cols[$keys[$x]]['type']) {
			case 'date':
				if (empty($values[$x])) {
					$values[$x] = "NULL";
				}
				else {
					$values[$x] = date('Y-m-d', strtotime($values[$x]));
				}
			break;
			case 'datetime':
				if (empty($values[$x])) {
					$values[$x] = "NULL";
				}
				else {
					$values[$x] = date('Y-m-d H:i:00', strtotime($values[$x]));
				}
			break;
			case 'time':
				if (empty($values[$x])) {
					$values[$x] = "NULL";
				}
				else {
					$values[$x] = date('H:i:00', strtotime($values[$x]));
				}
			break;
		}
	}
	
	$keys = "`".implode("`, `", $keys)."`";
	$values = '"'.implode('", "', array_map('escape', $values)).'"';
	$values = str_replace('"NULL"', "NULL", $values);
	
	$sql = "INSERT INTO `$type` ($keys) VALUES ($values)";

	if (!$duplicate_col) {
		if (q($sql)) {
			if ($type == 's_types') {
				$slug = $data['slug'];
				q("CREATE TABLE IF NOT EXISTS `$slug`( id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(id));");
			}
			if ($type == 's_columns') {
				$table = select_row("slug", "s_types", "id='{$data['table']}'", "");
				q("ALTER TABLE `$table` ADD `{$data['slug']}` ".to_mysql_col_type($data['type'], $data['length']));
			}
		}
	}
	else {
		notify("Column already exists.", 4, "Error");
	}
}

unset($_SESSION['data']);
unset($_SESSION['the_cols']);

$type_name = select_row("name", "s_types", "slug='$type'", "");
$type_icon = select_row("icon", "s_types", "slug='$type'", "");
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?=capitalize($type_name)." - ".$site_title?></title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- JQuery UI CSS -->
	<link href="vendor/jquery-ui/jquery-ui.min.css" rel="stylesheet">
	<link href="vendor/jquery-ui/jquery-ui.theme.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
	<link href="vendor/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
	
	<!-- DataTables Buttons -->
	<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet">

	<!-- DataTables TableTools CSS -->
	<link href="vendor/datatables-extensions/TableTools-2.2.4/css/dataTables.tableTools.min.css" rel="stylesheet">
	
	<!-- jQuery DateTimePicker -->
	<link href="vendor/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
	
    <!-- Custom CSS -->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <!--<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js"></script>
  	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/v4-shims.js"></script>
	
	<!-- Custom CSS -->
	<link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
		<?php require_once 'templates/nav.php'; ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?=($type_icon ? '<i class="fa fa-'.$type_icon.' fa-fw"></i> ' : '').capitalize($type_name)?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-lg-12">
					<div class="notify"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
							<span class=""><?=capitalize($type_name)?> List</span>
							<span class="pull-right"><a href="?action=add&type=<?=($type)?>" class="btn btn-success new-btn">New <?=capitalize($type_s)?></a></span>
                        </div>
                        <!-- /.panel-heading -->
						<div class="col_filters table container-fluid">
							<?php
								/*
								*	Build the column filters
								*/
								$editable_id = select_row("slug, id", "s_columns", "`table`=(SELECT id FROM s_types WHERE slug='$type') && slug = 'id'", "");
								
								$cols = select("id, name, slug, type, `table`, (SELECT `order` FROM `s_column-categories` cc WHERE cc.id=c.category) AS cat_order, disabled", "s_columns c", "`table`=(SELECT id FROM s_types WHERE slug='$type') AND slug <> 'reserved' AND hide = 'No'", "ORDER BY cat_order, `order` ASC LIMIT $max_columns");

								if (!$editable_id) {
									$cols2[] = array('name' => 'id', 'slug' => 'id', 'type' => 'number');
									$cols = array_merge($cols, $cols2);
								}

								for($x=0; $x < count($cols); $x++) {
									echo "<div class='row'>";
									
									if ($cols[$x] && ($editable_id ? true : $cols[$x]['slug'] != 'id') && $cols[$x]['slug'] != 'password' && $cols[$x]['slug'] != 'token') {
										echo "<div class='col-lg-4 col-xs-12'>";
										echo "<label>".capitalize($cols[$x]['name'])."</label><p id='col_filter_$x' class='col_filter'></p>";
										echo "</div>";
										$x++;
									}

									if ($cols[$x] && ($editable_id ? true : $cols[$x]['slug'] != 'id') && $cols[$x]['slug'] != 'password' && $cols[$x]['slug'] != 'token') {
										echo "<div class='col-lg-4 col-xs-12'>";
										echo "<label>".capitalize($cols[$x]['name'])."</label><p id='col_filter_$x' class='col_filter'></p>";
										echo "</div>";
										$x++;
									}
									
									if ($cols[$x] && ($editable_id ? true : $cols[$x]['slug'] != 'id') && $cols[$x]['slug'] != 'password' && $cols[$x]['slug'] != 'token') {
										echo "<div class='col-lg-4 col-xs-12'>";
										echo "<label>".capitalize($cols[$x]['name'])."</label><p id='col_filter_$x' class='col_filter'></p>";
										echo "</div>";
									}
									
									echo "</div>";
									
								}
							?>
						</div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTable-<?=$action.'-'.$type?>">
                                    <thead>
                                        <tr>
											<?php
											/*
											*	Build the list table
											*/
											foreach ($cols as $col) {
												if ($col['slug'] != 'password' && $col['slug'] != 'token' && ($editable_id ? true : $col['slug'] != 'id')) {
													echo "<th>".capitalize($col['name'])."</th>";
												}
												$c['name'][] = $col['name'];
												$c['slug'][] = $col['slug'];
												$c['type'][] = $col['type'];
												$c['table'][] = $col['table'];
											}
											$cs = "`".implode('`, `', $c['slug'])."`";
											?>
											<th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
										$args = select_row("conditions, orderby, sortorder, `limit`", "s_types", "slug='$type'", "");
										$default_order = select_row("value", "`s_site-settings`", "slug='default-order'", "");

										/*
										*	Get table data
										*/
										$rows = q("SELECT id, $cs FROM `$type` ".($args['conditions'] ? "WHERE {$args['conditions']}" : '')." ".($args['orderby'] ? "ORDER BY `{$args['orderby']}`" : 'ORDER BY `'.$cols[0]['slug'])."` ".($args['sortorder'] ? $args['sortorder'] : $default_order)." ".($args['limit'] ? "LIMIT {$args['limit']}" : ''));
										
										/*
										*	Build table rows
										*/
										if ($rows) {
											foreach ($rows as $row) {
												echo '<tr class="gradeA">';
												foreach ($cols as $col) {
													if ($col['slug'] != 'password' && $col['slug'] != 'token' && ($editable_id ? true : $col['slug'] != 'id')) {
														switch($col['type']) {
															default:
																echo '<td class="editable" id="'.$row['id'].'-'.$col['id'].'">'.($row[$col['slug']] ? $row[$col['slug']] : 'N/A').'</td>';
															break;
															case "select":
																$selected_value_cols = select_row("text, from_table", "s_selects", "col_id='{$col['id']}'", "");
																if ($selected_value_cols) {
																	$from_table = select_row("slug", "s_types", "id='{$selected_value_cols['from_table']}'", "");
																	$selected_value = select_row("`{$selected_value_cols['text']}`", "`$from_table`", "id='{$row[$col['slug']]}'", "");
																	echo '<td class="editable" id="'.$row['id'].'-'.$col['id'].'">'.($selected_value ? $selected_value : 'N/A').'<input type="hidden" value="'.$row[$col['slug']].'"></td>';
																}
																else {
																	echo '<td>Select column is not set up. <a href="'.$domain.'?action=add&type=s_selects&col_id='.$col['id'].'">Set one up here.</a></td>';
																}
															break;
															case "date":
																echo '<td class="editable" id="'.$row['id'].'-'.$col['id'].'">'.(is_null($row[$col['slug']]) ? '' : date('F j, Y', strtotime($row[$col['slug']]))).'</td>';
															break;
															case "datetime":
																echo '<td class="editable" id="'.$row['id'].'-'.$col['id'].'">'.(is_null($row[$col['slug']]) ? '' : date('F j, Y g:i a', strtotime($row[$col['slug']]))).'</td>';
															break;
															case "time":
																echo '<td class="editable" id="'.$row['id'].'-'.$col['id'].'">'.(is_null($row[$col['slug']]) ? '' : date('g:i a', strtotime($row[$col['slug']]))).'</td>';
															break;
														}
													}
												}
												echo '<td><a href="?action=edit&type='.$type.'&id='.$row['id'].'" class="btn btn-sm btn-primary">Edit</a> '.($type != 'site-settings' ? '<a onclick="if ( confirm(\'Are you sure you want to delete this row?\')) { $.ajax({ url: \''.$domain.'php/ajax.php?action=delete&type='.$type.'&row_id='.$row['id'].'\', success: function() { $(\'#'.$row['id'].'-'.$col['id'].'\').parent().fadeOut(); notify(\'Deleted 1 row.\', 2.5, \'Success\'); generate_datatable(); }, error: function() { notify(\'Failed to delete row.\', 4, \'Error\'); } }) } else { return false; }" class="btn btn-sm btn-danger">Delete</a>' : '').'</td>';
												echo '</tr>';
												
											}
										}
										?>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

	<!-- JQuery UI JavaScript -->
	<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
	
	<!-- DataTables Column Filter JavaScript -->
	<script src="vendor/datatables-plugins/columnfilter/jquery.dataTables.columnFilter.js"></script>
	
	<!-- DataTables DataTools JavaScript -->
	<script src="vendor/datatables-extensions/TableTools-2.2.4/js/dataTables.tableTools.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
	
	<!-- jQuery DateTimePicker -->
	<script src="vendor/datetimepicker/jquery.datetimepicker.js"></script>
	
    <!-- Custom Theme JavaScript -->
    <script src="js/sb-admin-2.js"></script>
	
	<script src="js/custom.js"></script>
	
	<?php notify_write(); ?>
	
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
		$.datepicker.regional[""].dateFormat = 'MM d, yy';
        $.datepicker.setDefaults($.datepicker.regional['']);
		
		var table;

		/*
		*	Generate datatable upon page load
		*/
		function generate_datatable() {
			
			if (table) {
				table.fnDestroy();	
			}
			
			var tableToolsButtonsOptions = {
				bSelectedOnly: true,
				oSelectorOpts: {
					filter: "applied"
				}
			};	
			
			table = $('#dataTable-<?=$action.'-'.$type?>').dataTable({
				dom: 'T<"clear">lBfrtip',
				responsive: true,
				stateSave: true,
				buttons: [     
					'colvis',       
					'copyHtml5',
					'excelHtml5',
					'csvHtml5',
					'pdfHtml5'
				],
				"bStateSave": true
			}).columnFilter({
				sPlaceHolder: "head:before",
				aoColumns: [
					<?php
						for($x=0; $x < count($cols); $x++) {
							if (($editable_id ? true : $cols[$x]['slug'] != 'id') && $cols[$x]['slug'] != 'password' && $cols[$x]['slug'] != 'token') {
								switch($cols[$x]['type']) {
									default:
									case "time":
										echo "{ sSelector: '#col_filter_$x', type: 'text' },";
									break;
									case "date":
									case "datetime":
										echo "{ sSelector: '#col_filter_$x', type: 'date-range' },";
									break;
									case "text":
									case "textarea":
										echo "{ sSelector: '#col_filter_$x', type: 'text'},";
									break;
									case "number":
									case "decimal":
										echo "{ sSelector: '#col_filter_$x', type: 'number-range' },";
									break;
									case "select":
										echo "{ sSelector: '#col_filter_$x', type: 'select', values: [";
												$selects = select_row("text, from_table", "s_selects", "col_id='{$cols[$x]['id']}'", "");
												if ($selects['text']) {
													$from_table = select_row("slug", "s_types", "id='{$selects['from_table']}'", "");
													
													$values = select('DISTINCT(`'.$selects['text'].'`)', "`$from_table`", "id IN (SELECT `{$cols[$x]['slug']}` FROM `$type`)", "ORDER BY `{$selects['text']}` ASC");
													$can_have_na = select_row("`no-answer`", "s_columns", "id='{$cols[$x]['id']}'", "");
													
													if ($values) {
														if ($can_have_na == 'Yes') {
															$vals[] = 'N/A';
														}														
														foreach ($values as $value) {
															$vals[] = addslashes($value[$selects['text']]);
														}
														$vs = "'".implode("', '", $vals)."'";
														echo $vs;
														unset($vals);
													}
												}
												
										echo "]},";									
									break;
									case "select_format":
										echo "{ sSelector: '#col_filter_$x', type: 'select', values: ['bool', 'date', 'datetime', 'decimal', 'number', 'select', 'text', 'textarea', 'time'] },";
									break;
									case "bool":
										echo "{ sSelector: '#col_filter_$x', type: 'select', values: ['Yes', 'No'] },";
									break;
								}
							}
						}
					?>
				]
			});

			/*
			*	Add export buttons
			*/
			new $.fn.dataTable.Buttons( table, {
				buttons: [
					'copy', 'excel', 'pdf'
				]
			} );
			
			var oSettings = table.fnSettings();

			for(iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {	
				oSettings.aoPreSearchCols[ iCol ].sSearch = "";
				oSettings.aoPreSearchCols[ iCol ].bRegex = false;
				oSettings.aoPreSearchCols[ iCol ].bSmart= false;
			}
		}
		
		generate_datatable();
		
		/*
		*	Make datatable editable after loading
		*/
		$('#dataTable-<?=$action.'-'.$type?>').on( 'draw.dt', function () {
			make_clickable();
		});
		
		var clickable = true;
		var the_val;
		
		make_clickable();
		
		/*
		*	Make row values editable via clicking
		*/
		function make_clickable() {
			$('.editable').click(function(e){  
				var id=$(this).attr('id');
				e.stopPropagation();
				
				if (!($(this).find('input[type=text]').length || $(this).find('input[type=number]').length || $(this).find('select').length || $(this).find('textarea').length) && clickable) {
					updateVal('#'+id);
				}
			});
		}
		/*
		*	Update value in the database if different from previous value
		*/
		function updateVal(currentEle) {
			if ($(currentEle).text() == 'N/A') {
				$(currentEle).text('');
			}
			
			currentVal = $(currentEle).text();
			var currentText = currentVal;

			clickable = false;
			
			var row_id = currentEle.split('-', 1)[0].substring(1);
			var col_id = currentEle.split("-").pop();

			var cols = {
				<?php
					$count = 0;
					foreach ($cols as $col) {
						if ($col['id']) {
							echo "'".$col['id']."': {type: '".$col['type']."', disabled: '".$col['disabled']."'},\n\t\t\t\t";
							$count++;
						}
					}
				?>
			};
			
			if (cols[col_id]['disabled'] === 'No') {
			
				switch(cols[col_id]['type']) {
					case "select":
						the_val = $(currentEle+' input[type=hidden]').val();
						currentVal = the_val;
						
						$.ajax({
							url: "<?=$domain?>php/ajax.php?action=select&col_id="+col_id+"&v="+the_val+"&class=thVal select_box",
							success: function(data) {
										$(currentEle).html(data);
										set_unfocus();
										$(".thVal").focus();
									},
							error: function(e, data, data2) {
										$(currentEle).html(the_val);
										notify(data+"\n"+data2, 5, 'Error');
							}
						});
					break;
					case "select_format":
						$(currentEle).html('<select class="form-control thVal"><option value="bool" '+($(currentEle).text() == 'bool' ? 'selected' : '')+'>bool</option><option value="date" '+($(currentEle).text() == 'date' ? 'selected' : '')+'>date</option><option value="datetime" '+($(currentEle).text() == 'datetime' ? 'selected' : '')+'>datetime</option><option value="decimal" '+($(currentEle).text() == 'decimal' ? 'selected' : '')+'>decimal</option><option value="number" '+($(currentEle).text() == 'number' ? 'selected' : '')+'>number</option><option value="select" '+($(currentEle).text() == 'select' ? 'selected' : '')+'>select</option><option value="text" '+($(currentEle).text() == 'text' ? 'selected' : '')+'>text</option><option value="textarea" '+($(currentEle).text() == 'textarea' ? 'selected' : '')+'>textarea</option><option value="time" '+($(currentEle).text() == 'time' ? 'selected' : '')+'>time</option></select>');
					break;
					case "decimal":
					case "number":
						$(currentEle).html('<input class="thVal" type="number" value="'+$(currentEle).text()+'" />');
					break;
					case "text":
						$(currentEle).html('<input class="thVal" type="text" value="'+$(currentEle).text()+'" />');
					break;
					case "textarea":
						$(currentEle).html('<textarea class="thVal">'+$(currentEle).text()+'</textarea>');
					break;
					case "bool":
						$(currentEle).html('<select class="thVal"><option value="Yes" '+($(currentEle).text() == 'Yes' ? 'selected' : '')+'>Yes</option><option value="No" '+($(currentEle).text() == 'No' ? 'selected' : '')+'>No</option></select>');
					break;
					case 'date':
						$(currentEle).html('<input type="text" class="thVal date" value="'+$(currentEle).text()+'">');
						
						$('.date').datetimepicker({
							timepicker: false,
							format: 'F j, Y'
						});
					break;
					case 'datetime':
						$(currentEle).html('<input type="text" class="thVal datetime" value="'+$(currentEle).text()+'">');
						
						$('.datetime').datetimepicker({
							format: 'F j, Y g:i a',
							formatTime:'g:i a',
							step: 15
						});
					break;
					case 'time':
						$(currentEle).html('<input type="text" class="thVal time" value="'+$(currentEle).text()+'">');
						
						$('.time').datetimepicker({
							datepicker: false,
							format: 'g:i a',
							formatTime:'g:i a',
							step: 15
						});
					break;
				}
				
				
				$(".thVal").focus();

				
				set_unfocus();
			
			}
			else {
				clickable = true;
			}

			/*
			*	Set table cell to unfocused with text after clicking out and updating value
			*/
			function set_unfocus() {
				
				function unfocus(ele) {
					if ($(ele).val() != currentVal && confirm("Confirm this edit?")) {
						$.ajax({
							url: "<?=$domain?>php/ajax.php?action=edit&table=<?=$type?>&col_id="+col_id+"&row_id="+row_id+"&v="+$(ele).val()+"&old_v="+currentVal+"&t="+cols[col_id]['type'],
							success: function(data) {
								if (data == '') {
									$(currentEle).html('N/A');
								}
								else {
									$(currentEle).html(data);
								}
								the_val = $(currentEle+' input[type=hidden]').val();
								notify("Updated Successfully!", 2.5, "Success");
								generate_datatable();
								clickable = true;	
							},
							error: function() {
								$(currentEle).html(currentVal);
								notify("Failed to update value.", 2.5, "Error");
								clickable = true;							
							}
						});
					}
					else {
						if ($(ele).hasClass('select_box')) {
							$(currentEle).html(currentText);
						}
						else {
							if (currentVal == '') {
								$(currentEle).html('N/A');
							}
							else {
								$(currentEle).html(currentVal);
							}
						}
						clickable = true;
					}
				}				
				
				$(".thVal").blur(function () {
					unfocus(this);
				});
				
				/*$(".thVal").keyup(function (event) {
					if (event.keyCode == 13) {
						unfocus(this);
					}
				});*/
				
			}
		}
    });
    </script>

</body>

</html>