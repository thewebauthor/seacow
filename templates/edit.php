<?php
/*
*	Edit page template for tables, columns, and rows
*	Author: Daniel Harris @thewebauthor
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?=$site_title?></title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
	
    <!-- Custom CSS -->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <!--<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js"></script>
  	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/v4-shims.js"></script>
	
	<!-- jQuery DateTimePicker -->
	<link href="vendor/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
	
	<!-- Custom CSS -->
	<link href="css/custom.css" rel="stylesheet">
	
    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<style>
	table.table tr td:last-child {
		width: 50%;
	}
	.table>tbody>tr {
		/*border: 1px solid #ddd;*/
	}
	</style>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
		<?php require_once 'templates/nav.php'; ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit <?=capitalize($type_s)?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

			<?php
				$cols_v = select("id, name, slug, type", "s_columns", "`table`=(SELECT view_table FROM s_views WHERE parent_table=(SELECT id FROM s_types WHERE slug='$type'))", "ORDER BY `order` ASC");

				if ($cols_v) {
					echo "<div class='container-fluid'><div class='row'><div class='col-12'><table class='col-lg-6 col-xs-12'><h3 id='view_tbl_name'></h3>";

					foreach ($cols_v as $col){
						$cv[] = $col['slug'];
						$cvt[$col['slug']] = [$col['type'], $col['id']];
					}
					
					$clsv = "`".implode('`, `', $cv)."`";
	
					$view = select_row("*", "s_views", "parent_table=(SELECT id FROM s_types WHERE slug='$type')", "");
					$view_table = select_row("slug, singular, name", "s_types", "id=(SELECT view_table FROM s_views WHERE parent_table=(SELECT id FROM s_types WHERE slug='$type'))", "");
					
					$foreign_key_parent = select_row("slug", "s_columns", "id='{$view['foreign_key_parent']}'", "");
					$foreign_key_view = select_row("slug", "s_columns", "id='{$view['foreign_key_view']}'", "");

					$values = select($clsv, "`".$view_table['slug']."`", "`$foreign_key_view`=(SELECT `$foreign_key_parent` FROM `$type` WHERE id='{$_GET['id']}')", "");

					if (count($values)) {
						echo "<script>jQuery('#view_tbl_name').text('Associated {$view_table['name']}');</script>";
						
						if ($view['display_links'] === 'Yes') {						
							$display_text_col1 = select_row("slug", "s_columns", "id={$view['link_text1']}");
							$display_text_col2 = select_row("slug", "s_columns", "id={$view['link_text2']}");
							$display_text_col3 = select_row("slug", "s_columns", "id={$view['link_text3']}");
							$display_text_col4 = select_row("slug", "s_columns", "id={$view['link_text4']}");
							$the_id = '';
							$display_text1 = '';
							$display_text2 = '';
							$display_text3 = '';
							$display_text4 = '';
							
							echo "<tr><th>".capitalize($display_text_col1)."</th><th>".capitalize($display_text_col2)."</th><th>".capitalize($display_text_col3)."</th><th>".capitalize($display_text_col4)."</th><th></th></tr>";

							foreach ($values as $k => $v) {
								$the_id = $v['id'];
								echo "<td>".$v[$display_text_col1]."</td>";
								echo "<td>".$v[$display_text_col2]."</td>";
								echo "<td>".$v[$display_text_col3]."</td>";
								echo "<td>".$v[$display_text_col4]."</td>";
								echo "<td><a href='?action=edit&type={$view_table['slug']}&id={$the_id}'>Edit</a></td></tr>";
							}
						}
						else {
							$x = 0;
							foreach ($values as $vs) {
								echo "<tr>";
								foreach ($vs as $k => $v) {
									if ($cvt[$k][0] == 'select') {
										$select = select_row("text, from_table", "s_selects", "col_id='".$cvt[$k][1]."'", "");
										$from_table = select_row("slug", "s_types", "id='".$select['from_table']."'", "");
										$v = select_row($select['text'], $from_table, "id='".$vs['id']."'", "");
									}
									$x++;
									echo "<td><strong>".capitalize($k)."</strong></td><td>".$v."</td>";
									if ($x % 1 === 0) {
										echo "</tr><tr>";
									}
								}
								echo "</tr>";
							}
						}
						
						echo "</table></div></div></div>";						
					}


				}
			?>

			<form action="?action=preview&type=<?=$type?>&id=<?=$id?>" method="post" role="form" autocomplete="off">
				<?php
					$reserved = select_row("reserved", "s_types", "slug='$type'", "");

					$cols = select("id, name, slug, type, length, description, (SELECT name FROM `s_column-categories` cc WHERE cc.id=c.category) AS cat, (SELECT `order` FROM `s_column-categories` cc WHERE cc.id=c.category) AS cat_order, `order`, disabled", "s_columns c", "`table`=(SELECT id FROM s_types WHERE slug='$type') AND slug <> 'reserved' ".($reserved == 'Yes' ? '' : "AND reserved='No'"), "ORDER BY cat_order, `order` ASC");
					
					foreach ($cols as $col){
						$c[] = $col['slug'];
					}
					
					$cls = "`".implode('`, `', $c)."`";

					$values = $_SESSION['data'] ? $_SESSION['data'] : select('id, '.$cls, "`".$type."`", "id='$id'", "");
					$from_preview = $_SESSION['data'] ? true : false;
					
					foreach($cols as $col) {
						$cl[$col['cat']][] = $col;
					}
					
					foreach ($cl as $k => $c) {
						echo "<fieldset><legend>$k</legend>";
						
						for ($x = 0; $x < count($c); $x++) {
							
							echo '<div class="row">';
							
							$name = capitalize($c[$x]['name']);
							$field_type = $c[$x]['type'];
							$length = $c[$x]['length'];
							$description = $c[$x]['description'];
							
								echo '
									<div class="col-lg-6">
										<div class="form-group">
										
											<label>'.$name.'</label>';
											
											$val = $from_preview ? $values[$c[$x]['slug']] : $values[0][$c[$x]['slug']];
											
											switch($field_type) {
												case 'select':
													$selected_value = $from_preview ? $values[$c[$x]['slug']] : $values[0][$c[$x]['slug']];
													generate_select($c[$x]['id'], $type.'['.$c[$x]['slug'].']', capitalize($selected_value), $c[$x]['disabled']);								
												break;
												case 'select_format':
													generate_select_format($type.'['.$c[$x]['slug'].']', $val, $c[$x]['disabled']);
												break;
												case 'text':
													echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
												break;
												case 'textarea':
													echo '<textarea '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' name="'.$type.'['.$c[$x]['slug'].']" class="form-control ckeditor" rows="6">'.$val.'</textarea>';
												break;
												case 'number':
													echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="number" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
												break;
												case 'decimal':
													echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
												break;
												case 'bool':
													echo '<select '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' name="'.$type.'['.$c[$x]['slug'].']" class="form-control">';
														echo '<option value="Yes" '.($val === 'Yes' ? 'selected' : '').'>Yes</option>';
														echo '<option value="No" '.($val === 'No' ? 'selected' : '').'>No</option>';
													echo '</select>';
												break;
												case 'password':
													echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="password" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
												break;
												case 'date':
													echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control date" value="'.(empty($val) ? '' : date('F j, Y', strtotime($val))).'">';
												break;
												case 'datetime':
													echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control datetime" value="'.(empty($val) ? '' : date('F j, Y g:i a', strtotime($val))).'">';
												break;
												case 'time':
													echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control time" value="'.(empty($val) ? '' : date('g:i a', strtotime($val))).'">';
												break;
											}
											echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][id]" value="'.$c[$x]['id'].'">';
											echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][type]" value="'.$field_type.'">';
											echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][name]" value="'.$name.'">';
											echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][cat]" value="'.$k.'">';
											echo '<p class="help-block">'.$description.'</p>
										</div>
									</div>
									';
									
									$x++;
									
									$name = capitalize($c[$x]['name']);
									$field_type = $c[$x]['type'];
									$length = $c[$x]['length'];
									$description = $c[$x]['description'];
									$val = $from_preview ? $values[$c[$x]['slug']] : $values[0][$c[$x]['slug']];
									
									if ($name) {	
									
										echo'
										<div class="col-lg-6">
											<div class="form-group">';

											echo '<label>'.$name.'</label>';
												
												switch($field_type) {
													case 'select':
														$selected_value = $from_preview ? $values[$c[$x]['slug']] : $values[0][$c[$x]['slug']];
														generate_select($c[$x]['id'], $type.'['.$c[$x]['slug'].']', capitalize($selected_value), $c[$x]['disabled']);							
													break;
													case 'select_format':
														generate_select_format($type.'['.$c[$x]['slug'].']', $val, $c[$x]['disabled']);
													break;
													case 'text':
														echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
													break;
													case 'textarea':
														echo '<textarea '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' name="'.$type.'['.$c[$x]['slug'].']" class="form-control ckeditor" rows="6">'.$val.'</textarea>';
													break;
													case 'number':
														echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="number" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
													break;
													case 'decimal':
														echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
													break;
													case 'bool':
														echo '<select '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' name="'.$type.'['.$c[$x]['slug'].']" class="form-control">';
															echo '<option value="Yes" '.($val === 'Yes' ? 'selected' : '').'>Yes</option>';
															echo '<option value="No" '.($val === 'No' ? 'selected' : '').'>No</option>';
														echo '</select>';
													break;
													case 'password':
														echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="password" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
													break;
													case 'date':
														echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control date" value="'.(empty($val) ? '' : date('F j, Y', strtotime($val))).'">';
													break;
													case 'datetime':
														echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control datetime" value="'.(empty($val) ? '' : date('F j, Y g:i a', strtotime($val))).'">';
													break;
													case 'time':
														echo '<input '.($c[$x]['disabled'] === 'Yes' ? 'readonly' : '').' type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control time" value="'.(empty($val) ? '' : date('g:i a', strtotime($val))).'">';
													break;
												}
												echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][id]" value="'.$c[$x]['id'].'">';
												echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][type]" value="'.$field_type.'">';
												echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][name]" value="'.$name.'">';
												echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][cat]" value="'.$k.'">';
												echo '<p class="help-block">'.$description.'</p>
											</div>
										</div>';
									}
								echo '</div>';
						}
						echo "</fieldset>";
					}
				?>
				<div class="row">
					<div class="col-lg-12">
						<input type="reset" class="btn btn-danger pull-right">
						<button type="submit" name="edit_<?=$type?>" value="<?=$id?>" class="btn btn-primary pull-right">Submit</button>
					</div>
				</div>
			</form>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	
    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

	<!-- jQuery DateTimePicker -->
	<script src="vendor/datetimepicker/jquery.datetimepicker.js"></script>
	
	<script>
	$('.date').datetimepicker({
		timepicker: false,
		format: 'F j, Y'
	});
	$('.datetime').datetimepicker({
		format: 'F j, Y g:i a',
		formatTime:'g:i a',
		step: 15
	});
	$('.time').datetimepicker({
		datepicker: false,
		format: 'g:i a',
		formatTime:'g:i a',
		step: 15
	});
	</script>
	
	<!-- CKEditor -->
	<script src="js/ckeditor/ckeditor.js"></script>
	
    <!-- Custom Theme JavaScript -->
	<script src="js/sb-admin-2.js"></script>
	
	<script src="js/custom.js"></script>
	
	<script>
		prevent_unsaved_edits_loss(<?=$from_preview ? 'true' : 'false'?>);
	</script>

</body>

</html>
<?php
unset($_SESSION['data']);
?>