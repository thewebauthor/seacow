<?php
/*
*	Add page template for tables, columns, and rows
*	Author: Daniel Harris @thewebauthor
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?=$site_title?></title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">	

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <!--<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js"></script>
  	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/v4-shims.js"></script>
	
	<!-- jQuery DateTimePicker -->
	<link href="vendor/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
	
	<!-- Custom CSS -->
	<link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
		<?php require_once 'templates/nav.php'; ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">New <?=capitalize($type_s)?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
			<form action="?action=preview&type=<?=$type?>&id=<?=$id?>" method="post" role="form" autocomplete="off">
				<?php
					$reserved = select_row("reserved", "s_types", "slug='$type'", "");

					$cols = select("id, name, slug, type, length, description, (SELECT name FROM `s_column-categories` cc WHERE cc.id=c.category) AS cat, (SELECT `order` FROM `s_column-categories` cc WHERE cc.id=c.category) AS cat_order, `order`", "s_columns c", "`table`=(SELECT id FROM s_types WHERE slug='$type') AND slug <> 'reserved' ".($reserved == 'Yes' ? '' : "AND reserved='No'"), "ORDER BY cat_order, `order` ASC");

					$values = $_SESSION['data'] ? $_SESSION['data'] : '';
					$from_preview = $_SESSION['data'] ? true : false;
					
					foreach($cols as $col) {
						$cl[$col['cat']][] = $col;
					}
					
					foreach ($cl as $k => $c) {
						echo "<fieldset><legend>$k</legend>";
					
						for ($x = 0; $x < count($c); $x++) {
							echo '<div class="row">';
							
							$name = capitalize($c[$x]['name']);
							$field_type = $c[$x]['type'];
							$length = $c[$x]['length'];
							$description = $c[$x]['description'];
							$val = $from_preview ? $values[$c[$x]['slug']] : '';
							
								echo '
									<div class="col-lg-6">
										<div class="form-group">
										
											<label>'.$name.'</label>';
											
											switch($field_type) {
												case 'select':
													generate_select($c[$x]['id'], $type.'['.$c[$x]['slug'].']', $val);								
												break;
												case 'select_format':
													generate_select_format($type.'['.$c[$x]['slug'].']', $val);
												break;
												case 'text':
													echo '<input type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
												break;
												case 'textarea':
													echo '<textarea name="'.$type.'['.$c[$x]['slug'].']" class="form-control ckeditor" rows="3">'.$val.'</textarea>';
												break;
												case 'number':
													echo '<input type="number" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.($val ? $val : ($type == 's_selects' && $c[$x]['slug'] == 'col_id' ? $col_id : '')).'">';
												break;
												case 'decimal':
													echo '<input type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
												break;
												case 'bool':
													echo '<select name="'.$type.'['.$c[$x]['slug'].']" class="form-control">';
														echo '<option value="Yes" '.($val === 'Yes' ? 'selected' : '').'>Yes</option>';
														echo '<option value="No" '.($val === 'No' ? 'selected' : ($val ? '' : 'selected')).'>No</option>';
													echo '</select>';
												break;
												case 'password':
													echo '<input type="password" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
												break;
												case 'date':
													echo '<input type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control date" value="'.$val.'">';
												break;
												case 'datetime':
													echo '<input type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control datetime" value="'.$val.'">';
												break;
												case 'time':
													echo '<input type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control time" value="'.$val.'">';
												break;
											}
											echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][id]" value="'.$c[$x]['id'].'">';
											echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][type]" value="'.$field_type.'">';
											echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][name]" value="'.$name.'">';
											echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][cat]" value="'.$k.'">';
											echo '<p class="help-block">'.$description.'</p>
										</div>
									</div>
									';
									
									$x++;
									$name = capitalize($c[$x]['name']);
									$field_type = $c[$x]['type'];
									$length = $c[$x]['length'];
									$description = $c[$x]['description'];
									$val = $from_preview ? $values[$c[$x]['slug']] : '';
									
									if ($name) {	
									
										echo'
										<div class="col-lg-6">
											<div class="form-group">';

											echo '<label>'.$name.'</label>';
												
												switch($field_type) {
													case 'select':
														generate_select($c[$x]['id'], $type.'['.$c[$x]['slug'].']', $val);									
													break;
													case 'select_format':
														generate_select_format($type.'['.$c[$x]['slug'].']', $val);
													break;
													case 'text':
														echo '<input type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
													break;
													case 'textarea':
														echo '<textarea name="'.$type.'['.$c[$x]['slug'].']" class="form-control ckeditor" rows="3">'.$val.'</textarea>';
													break;
													case 'number':
														echo '<input type="number" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
													break;
													case 'decimal':
														echo '<input type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
													break;
													case 'bool':
														echo '<select name="'.$type.'['.$c[$x]['slug'].']" class="form-control">';
															echo '<option value="Yes" '.($val === 'Yes' ? 'selected' : '').'>Yes</option>';
															echo '<option value="No" '.($val === 'No' ? 'selected' : ($val ? '' : 'selected')).'>No</option>';
														echo '</select>';
													break;
													case 'password':
														echo '<input type="password" name="'.$type.'['.$c[$x]['slug'].']" class="form-control" value="'.$val.'">';
													break;
													case 'date':
														echo '<input type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control date" value="'.$val.'">';
													break;
													case 'datetime':
														echo '<input type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control datetime" value="'.$val.'">';
													break;
													case 'time':
														echo '<input type="text" name="'.$type.'['.$c[$x]['slug'].']" class="form-control time" value="'.$val.'">';
													break;
												}
												echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][id]" value="'.$c[$x]['id'].'">';
												echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][type]" value="'.$field_type.'">';
												echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][name]" value="'.$name.'">';
												echo '<input type="hidden" name="the_cols['.$c[$x]['slug'].'][cat]" value="'.$k.'">';
												echo '<p class="help-block">'.$description.'</p>
											</div>
										</div>';
									}
								echo '</div>';
						}
						echo "</fieldset>";
					}
				?>
				<div class="row">
					<div class="col-lg-12">
						<input type="reset" class="btn btn-danger pull-right">
						<button type="submit" name="add_<?=$type?>" value="1" class="btn btn-primary pull-right">Submit</button>
					</div>
				</div>
			</form>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	
    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

	<!-- jQuery DateTimePicker -->
	<script src="vendor/datetimepicker/jquery.datetimepicker.js"></script>
	
	<script>
	$('.date').datetimepicker({
		timepicker: false,
		format: 'F j, Y'
	});
	$('.datetime').datetimepicker({
		format: 'F j, Y g:i a',
		formatTime:'g:i a',
		step: 15
	});
	$('.time').datetimepicker({
		datepicker: false,
		format: 'g:i a',
		formatTime:'g:i a',
		step: 15
	});
	</script>
	
	<!-- CKEditor -->
	<script src="js/ckeditor/ckeditor.js"></script>
	
    <!-- Custom Theme JavaScript -->
	<script src="js/sb-admin-2.js"></script>
	
	<script src="js/custom.js"></script>
	
	<script>
		prevent_unsaved_edits_loss(<?=$from_preview ? 'true' : 'false'?>);
	</script>

</body>

</html>
<?php
unset($_SESSION['data']);
?>