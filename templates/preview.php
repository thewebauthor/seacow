<?php
/*
*	Preview data template after editing or adding
*	Author: Daniel Harris @thewebauthor
*/

$data = $$type;
$_SESSION['data'] = $data;
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?=$site_title?></title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <!--<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">-->
	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js"></script>
  	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/v4-shims.js"></script>
	
	<!-- Custom CSS -->
	<link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
		<?php require_once 'templates/nav.php'; ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Preview <?=capitalize($type_s)?> Changes</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
			<?php
			/*
			*	Get columns and output new values
			*/
			if ($type == 's_types') {
				$old_slug = select_row("slug", "s_types", "id='$id'", "");
				$args = "old_slug=$old_slug";
			}
			if ($type == 's_columns') {
				$old_col_name = select_row("slug", "s_columns", "id='$id'", "");
				$args = "old_col_name=$old_col_name";
			}

					foreach ($the_cols as $k => $the_col) {
						$cls[$the_col['cat']][] = array('id' => $the_col['id'], 'slug' => $k, 'name' => $the_col['name'], 'type' => $the_col['type'], 'value' => $data[$k]);
					}
					
					foreach ($cls as $k => $c) {
					
						echo "<fieldset><legend>$k</legend>";
						
						for($x = 0; $x < count($c); $x++){
							echo '<div class="row">';
							
								echo '
									<div class="col-lg-6">
										<div class="form-group">
										
											<label>'.capitalize($c[$x]['name']).'</label>';
											if ($c[$x]['type'] == 'select') {
												$select = select_row("`text`, from_table", "s_selects", "col_id='{$c[$x]['id']}'", "");
												if ($select['text']) {
													$from_table = select_row("slug", "s_types", "id='{$select['from_table']}'", "");
													$value = select_row('`'.$select['text'].'`', "`$from_table`", "id='".$c[$x]['value']."'", "");
												}
												if ($value) {
													echo '<pre>'.$value.'</pre>';
												}
												else {
													echo '<pre>N/A</pre>';
												}
											}
											else {
												if ($c[$x]) {
													echo '<pre>'.$c[$x]['value'].'</pre>';
												}
												else {
													echo '<pre>N/A</pre>';
												}
											}
											$_SESSION['the_cols'][$c[$x]['slug']]['type'] = $c[$x]['type'];
								echo '
										</div>
									</div>';
									
								$x++;

								echo '
									<div class="col-lg-6">
										<div class="form-group">
										
											<label>'.capitalize($c[$x]['name']).'</label>';
											if ($c[$x]['type'] == 'select') {
												$select = select_row("`text`, from_table", "s_selects", "col_id='{$c[$x]['id']}'", "");
												if ($select['text']) {
													$from_table = select_row("slug", "s_types", "id='{$select['from_table']}'", "");
													$value = select_row('`'.$select['text'].'`', "`$from_table`", "id='".$c[$x]['value']."'", "");
												}
												if ($value) {
													echo '<pre>'.$value.'</pre>';
												}
												else {
													echo '<pre>N/A</pre>';
												}
											}
											else {
												if ($c[$x]) {
													echo '<pre>'.$c[$x]['value'].'</pre>';
												}
												else {
													echo '<pre>N/A</pre>';
												}
											}
											$_SESSION['the_cols'][$c[$x]['slug']]['type'] = $c[$x]['type'];
								echo '
										</div>
									</div>';
							echo '</div>';
						}
						echo "</fieldset>";
					}
				?>
				<div class="row">
					<div class="col-lg-12">
						<?php
						if (${'add_'.$type}) {
							echo '<a href="?action=add&type='.$type.'" class="btn btn-warning pull-right">Edit</a>';
							echo '<a href="?action=list&type='.$type.'&add=1" class="btn btn-primary pull-right">Submit</a>';
						}
						if (${'edit_'.$type}) {
							echo '<a href="?action=edit&type='.$type.'&id='.$id.'" class="btn btn-warning pull-right">Edit</a>';
							echo '<a href="?action=list&type='.$type.'&edit='.$id.'&'.$args.'" class="btn btn-primary pull-right">Submit</a>';
						}
						?>
					</div>
				</div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	
	<!-- Metis Menu Plugin JavaScript -->
	<script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
	<script src="js/sb-admin-2.js"></script>
	
	<script src="js/custom.js"></script>

</body>

</html>