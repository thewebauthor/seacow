# Seacow

Seacow is a user-friendly MySQL database and content management system. Build and manage MySQL databases without knowing SQL. Seacow was designed from the ground up and was initially integrated into an e-commerce order/product management system. Seacow generates fully-relational MySQL databases on-the-fly.

### Logging in and Demo Site

Demo: http://seacow.atwebpages.com/
The default username and password is `admin` `admin`.

### Technologies

Seacow utilizes different open source projects to work properly:

* [PHP] - A server-side scripting language used to build web applications
* [MySQL] - An open-source database type
* [Twitter Bootstrap] - A UI boilerplate for modern web apps
* [jQuery] - A library for writing efficient JavaScript
* [SB Admin 2] - A Bootstrap-built backend boilerplate

### Installation

Seacow requires an Apache or Windows Server with PHP 5+ and MySQL 5 to run.

Example for setting up Apache, PHP, and MySQL on a Debian server.

```sh
$ sudo apt-get update
$ sudo apt-get install apache2
$ sudo apt-get install mysql-server libapache2-mod-auth-mysql php7.0-mysql
$ sudo mysql_install_db
$ sudo /usr/bin/mysql_secure_installation
$ sudo apt-get install php7.0 libapache2-mod-php7.0 php7.0-mcrypt
$ sudo service apache2 restart
```

### Cloning

```sh
$ cd /var/www/html
$ git clone https://github.com/TheWebAuthor/seacow.git
```

### Database Import

Import the `import_data.sql` file into your desired MySQL database. Add your database credentials in `/creds.php`.

### Debugging

PHP and MySQL errors are logged in `/errors.txt` and `/php/errors.txt`. Error notifications are displayed to the user when they occur.

[mysql]: https://www.mysql.com
[twitter bootstrap]: http://twitter.github.com/bootstrap/
[jquery]: http://jquery.com
[php]: http://php.net
[sb admin 2]: https://startbootstrap.com/template-overviews/sb-admin-2/
