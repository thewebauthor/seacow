-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2018 at 11:55 AM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.27-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `seacow`
--

-- --------------------------------------------------------

--
-- Table structure for table `s_column-categories`
--

CREATE TABLE `s_column-categories` (
  `id` int(11) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  `order` varchar(3) NOT NULL,
  `table` varchar(3) DEFAULT NULL,
  `reserved` varchar(3) NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_column-categories`
--

INSERT INTO `s_column-categories` (`id`, `name`, `order`, `table`, `reserved`) VALUES
(11, 'General', '0', NULL, 'Yes'),
(18, 'MySQL', '2', '7', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `s_columns`
--

CREATE TABLE `s_columns` (
  `id` int(4) NOT NULL,
  `name` varchar(50) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `length` varchar(5) DEFAULT NULL,
  `table` varchar(3) NOT NULL,
  `description` varchar(500) NOT NULL,
  `no-answer` varchar(3) NOT NULL DEFAULT 'No',
  `disabled` varchar(3) NOT NULL DEFAULT 'No',
  `order` int(4) NOT NULL,
  `category` varchar(3) NOT NULL DEFAULT '11',
  `reserved` varchar(3) NOT NULL DEFAULT 'No',
  `hide` varchar(3) NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_columns`
--

INSERT INTO `s_columns` (`id`, `name`, `slug`, `type`, `length`, `table`, `description`, `no-answer`, `disabled`, `order`, `category`, `reserved`, `hide`) VALUES
(31, 'username', 'username', 'text', '50', '6', 'User\'s username', 'No', 'No', 0, '11', 'Yes', 'No'),
(32, 'password', 'password', 'password', '32', '6', 'User\'s password', 'No', 'No', 0, '11', 'Yes', 'No'),
(33, 'email', 'email', 'text', '50', '6', 'User\'s email address', 'No', 'No', 0, '11', 'Yes', 'No'),
(34, 'token', 'token', 'text', '32', '6', 'User\'s token code when resetting passwords', 'No', 'No', 0, '11', 'Yes', 'Yes'),
(35, 'name', 'name', 'text', '100', '7', 'Name of the Type', 'No', 'No', 0, '11', 'Yes', 'No'),
(36, 'slug', 'slug', 'text', '50', '7', 'Lowercase name (No special characters i.e. !#@$%)', 'No', 'No', 0, '11', 'Yes', 'No'),
(37, 'singular', 'singular', 'text', '50', '7', 'Singular form of the Type', 'No', 'No', 0, '11', 'Yes', 'No'),
(38, 'icon', 'icon', 'text', '50', '7', 'Font Awesome icon to be used in the menu', 'No', 'No', 0, '11', 'Yes', 'No'),
(39, 'name', 'name', 'text', '50', '16', 'Name of the column', 'No', 'No', 3, '11', 'Yes', 'No'),
(40, 'type', 'type', 'select_format', '50', '16', 'Text format', 'No', 'No', 4, '11', 'Yes', 'No'),
(41, 'length', 'length', 'text', '10', '16', 'How many characters will the column be able to contain?', 'No', 'No', 7, '11', 'Yes', 'No'),
(42, 'table', 'table', 'select', '3', '16', 'Which table will this column be in?', 'No', 'No', 6, '11', 'Yes', 'No'),
(43, 'description', 'description', 'textarea', '500', '16', '', 'No', 'No', 8, '11', 'Yes', 'No'),
(69, 'Column ID', 'col_id', 'number', '10', '18', 'The select column ID', 'No', 'No', 0, '11', 'Yes', 'No'),
(56, 'order', 'order', 'number', '3', '7', 'Order of Type in menu (ascending order)', 'No', 'No', 0, '11', 'Yes', 'No'),
(71, 'From Table', 'from_table', 'select', '3', '18', 'Select data from which table?', 'No', 'No', 0, '11', 'Yes', 'No'),
(72, 'args', 'args', 'text', '100', '18', 'Arguments to exclude rows', 'No', 'No', 0, '11', 'Yes', 'No'),
(77, 'order', 'order', 'number', '4', '16', 'Order of the column in the Columns list', 'No', 'No', 2, '11', 'Yes', 'No'),
(79, 'Column Slug In From Table', 'text', 'text', '50', '18', 'The slug of the column to be displayed in the select options.', 'No', 'No', 4, '11', 'Yes', 'No'),
(111, 'reserved', 'reserved', 'bool', '3', '7', '', 'No', 'No', 10, '11', 'Yes', 'No'),
(112, 'reserved', 'reserved', 'bool', '3', '16', '', 'No', 'No', 0, '11', 'Yes', 'No'),
(134, 'hide', 'hide', 'bool', '3', '7', 'Hide this table from left side menu?', 'No', 'No', 0, '11', 'Yes', 'No'),
(129, 'hide', 'hide', 'bool', '3', '16', 'Hide this column from the list views', 'No', 'No', 8, '11', 'Yes', 'No'),
(135, 'category', 'category', 'select', '3', '7', 'Category of the Type (To be grouped in side menu)', 'Yes', 'No', 0, '11', 'Yes', 'No'),
(162, 'Conditions (MySQL)', 'conditions', 'text', '100', '7', 'Conditions in MySQL for the table', 'No', 'No', 1, '18', 'Yes', 'No'),
(172, 'category', 'category', 'select', '3', '16', 'Category of the column.', 'No', 'No', 6, '11', 'Yes', 'No'),
(174, 'Order', 'order', 'text', '3', '51', '', 'No', 'No', 2, '11', 'Yes', 'No'),
(177, 'slug', 'slug', 'text', '50', '16', 'Lowercase name of the column (No special characters i.e. !#@$%)', 'No', 'No', 3, '11', 'Yes', 'No'),
(187, 'Name', 'name', 'text', '50', '54', 'Name of the setting.', 'No', 'Yes', 1, '11', 'Yes', 'No'),
(188, 'Slug', 'slug', 'text', '50', '54', 'Lowercase name of setting (Spaces will be converted to hyphens)', 'No', 'Yes', 2, '11', 'Yes', 'No'),
(189, 'Value', 'value', 'text', '50', '54', 'The value of the setting.', 'No', 'No', 3, '11', 'Yes', 'No'),
(191, 'Has no answer?', 'no-answer', 'bool', '3', '16', 'Can contain a no answer value?', 'No', 'No', 7, '11', 'Yes', 'No'),
(192, 'Order By (MySQL)', 'orderby', 'text', '200', '7', 'MySQL order by column', 'No', 'No', 2, '18', 'Yes', 'No'),
(193, 'Sort Order (MySQL)', 'sortorder', 'text', '3', '7', 'Sort order of column (ASC or DESC)', 'No', 'No', 3, '18', 'Yes', 'No'),
(194, 'Limit (MySQL)', 'limit', 'text', '10', '7', 'Limit the number of rows to a number (1-999999999)', 'No', 'No', 4, '18', 'Yes', 'No'),
(195, 'Max Columns', 'max-columns', 'number', '2', '7', 'Max number of columns in list views', 'No', 'No', 0, '11', 'Yes', 'No'),
(198, 'Disabled', 'disabled', 'bool', '3', '16', 'Disable this column from editing?', 'No', 'No', 9, '11', 'Yes', 'No'),
(380, 'Reserved', 'reserved', 'bool', '3', '51', '', 'No', 'No', 0, '11', 'Yes', 'No'),
(401, 'Icon', 'icon', 'text', '50', '42', '', 'No', 'No', 2, '11', 'Yes', 'No'),
(400, 'Name', 'name', 'text', '50', '42', '', 'No', 'No', 1, '11', 'Yes', 'No'),
(402, 'Name', 'name', 'text', '50', '51', '', 'No', 'No', 1, '11', 'Yes', 'No'),
(404, 'Table', 'table', 'select', '3', '51', '', 'No', 'No', 4, '11', 'Yes', 'No'),
(430, 'Parent Table', 'parent_table', 'select', '3', '98', '', 'No', 'No', 1, '11', 'Yes', 'No'),
(431, 'View Table', 'view_table', 'select', '3', '98', '', 'No', 'No', 2, '11', 'Yes', 'No'),
(432, 'Foreign Key Parent Table', 'foreign_key_parent', 'select', '3', '98', '', 'No', 'No', 3, '11', 'Yes', 'No'),
(434, 'Foreign Key View Table', 'foreign_key_view', 'select', '3', '98', '', 'No', 'No', 4, '11', 'Yes', 'No'),
(440, 'Display as Links', 'display_links', 'bool', '3', '98', '', 'No', 'No', 5, '11', 'Yes', 'No'),
(441, 'Link Text 1', 'link_text1', 'select', '3', '98', '', 'Yes', 'No', 6, '11', 'Yes', 'No'),
(443, 'Link Text 2', 'link_text2', 'select', '3', '98', '', 'Yes', 'No', 7, '11', 'Yes', 'No'),
(444, 'Link Text 3', 'link_text3', 'select', '3', '98', '', 'Yes', 'No', 8, '11', 'Yes', 'No'),
(445, 'Link Text 4', 'link_text4', 'select', '3', '98', '', 'Yes', 'No', 9, '11', 'Yes', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `s_selects`
--

CREATE TABLE `s_selects` (
  `id` int(11) NOT NULL,
  `col_id` int(11) NOT NULL,
  `table` varchar(3) NOT NULL,
  `text` varchar(50) NOT NULL,
  `from_table` varchar(3) NOT NULL,
  `args` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_selects`
--

INSERT INTO `s_selects` (`id`, `col_id`, `table`, `text`, `from_table`, `args`) VALUES
(104, 71, '', 'name', '7', ''),
(105, 42, '', 'name', '7', ''),
(106, 172, '', 'name', '51', ''),
(107, 135, '', 'name', '42', ''),
(108, 404, '', 'Table', '7', ''),
(112, 430, '', 'name', '7', '`reserved`=\'No\' and `hide`=\'No\''),
(113, 431, '', 'name', '7', '`reserved`=\'No\' and `hide`=\'No\''),
(114, 432, '', 'name', '16', '`reserved`=\'No\' and `hide`=\'No\''),
(116, 434, '', 'name', '16', '`reserved`=\'No\' and `hide`=\'No\''),
(117, 441, '', 'name', '16', '`reserved`=\'No\' and `hide`=\'No\''),
(118, 443, '', 'name', '16', '`reserved`=\'No\' and `hide`=\'No\''),
(119, 444, '', 'name', '16', '`reserved`=\'No\' and `hide`=\'No\''),
(120, 445, '', 'name', '16', '`reserved`=\'No\' and `hide`=\'No\'');

-- --------------------------------------------------------

--
-- Table structure for table `s_site-settings`
--

CREATE TABLE `s_site-settings` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `slug` varchar(25) NOT NULL,
  `value` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `s_site-settings`
--

INSERT INTO `s_site-settings` (`id`, `name`, `slug`, `value`) VALUES
(1, 'Maximum number of login attempts', 'max-login-attempts', '5'),
(2, 'Default order of tables (ASC or DESC)', 'default-order', 'ASC'),
(3, 'Admin Email', 'admin-email', ''),
(4, 'Default max number of columns in list views', 'max-columns', '6'),
(5, 'Site Title', 'site-title', 'Default Site');

-- --------------------------------------------------------

--
-- Table structure for table `s_type-categories`
--

CREATE TABLE `s_type-categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `order` int(3) DEFAULT NULL,
  `icon` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `s_types`
--

CREATE TABLE `s_types` (
  `id` int(11) NOT NULL,
  `order` int(3) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `singular` varchar(50) NOT NULL,
  `category` int(3) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `max-columns` varchar(2) NOT NULL DEFAULT '6',
  `conditions` varchar(500) NOT NULL,
  `orderby` varchar(200) NOT NULL,
  `sortorder` varchar(3) NOT NULL,
  `limit` varchar(10) NOT NULL,
  `reserved` varchar(3) NOT NULL DEFAULT 'No',
  `hide` varchar(3) NOT NULL DEFAULT 'No',
  `show_id` varchar(3) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `s_types`
--

INSERT INTO `s_types` (`id`, `order`, `name`, `slug`, `singular`, `category`, `icon`, `max-columns`, `conditions`, `orderby`, `sortorder`, `limit`, `reserved`, `hide`, `show_id`) VALUES
(16, 3, 'Columns', 's_columns', 'column', 0, '', '6', '`table` NOT IN(SELECT id FROM s_types WHERE reserved=\'Yes\')', '', '', '', 'Yes', 'No', NULL),
(7, 1, 'Types', 's_types', 'type', 0, '', '6', 'reserved <> \'Yes\'', '', '', '', 'Yes', 'No', NULL),
(6, 6, 'Users', 's_users', 'user', 0, 'users', '6', '', '', '', '', 'Yes', 'No', NULL),
(18, 5, 'Selects', 's_selects', 'select', 0, '', '6', 'col_id NOT IN (SELECT id FROM `s_columns` WHERE `table` IN (SELECT id FROM `s_types` WHERE reserved=\'Yes\'))', '', '', '', 'Yes', 'No', NULL),
(42, 2, 'Type Categories', 's_type-categories', 'Type Category', 0, '', '6', '', '', '', '', 'Yes', 'Yes', NULL),
(51, 4, 'Column Categories', 's_column-categories', 'Column Category', 3, '', '6', 'reserved = \'No\'', '', '', '', 'Yes', 'Yes', NULL),
(54, 7, 'Site Settings', 's_site-settings', 'Setting', 3, 'cog', '6', '', '', '', '', 'Yes', 'No', NULL),
(98, 2, 'Views', 's_views', 'View', 12, '', '6', '', '', '', '', 'Yes', 'No', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `s_users`
--

CREATE TABLE `s_users` (
  `id` int(4) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(50) NOT NULL,
  `token` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `s_users`
--

INSERT INTO `s_users` (`id`, `username`, `password`, `email`, `token`) VALUES
(4, 'admin', '434d711276d2ed2068e7a02499fac15c', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `s_views`
--

CREATE TABLE `s_views` (
  `id` int(11) NOT NULL,
  `parent_table` varchar(3) DEFAULT NULL,
  `view_table` varchar(3) DEFAULT NULL,
  `foreign_key_parent` varchar(3) DEFAULT NULL,
  `foreign_key_view` varchar(3) DEFAULT NULL,
  `display_links` varchar(3) DEFAULT NULL,
  `link_text1` varchar(3) DEFAULT NULL,
  `link_text2` varchar(3) DEFAULT NULL,
  `link_text3` varchar(3) DEFAULT NULL,
  `link_text4` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `s_column-categories`
--
ALTER TABLE `s_column-categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_columns`
--
ALTER TABLE `s_columns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_selects`
--
ALTER TABLE `s_selects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_site-settings`
--
ALTER TABLE `s_site-settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_type-categories`
--
ALTER TABLE `s_type-categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `s_types`
--
ALTER TABLE `s_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `s_users`
--
ALTER TABLE `s_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `USERNAME` (`username`);

--
-- Indexes for table `s_views`
--
ALTER TABLE `s_views`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `s_column-categories`
--
ALTER TABLE `s_column-categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `s_columns`
--
ALTER TABLE `s_columns`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=446;

--
-- AUTO_INCREMENT for table `s_selects`
--
ALTER TABLE `s_selects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `s_site-settings`
--
ALTER TABLE `s_site-settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `s_type-categories`
--
ALTER TABLE `s_type-categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `s_types`
--
ALTER TABLE `s_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `s_users`
--
ALTER TABLE `s_users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `s_views`
--
ALTER TABLE `s_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
