<?php 
/*
*	Main controller
*	Author: Daniel Harris @thewebauthor
*/

error_reporting(E_ALL & ~E_NOTICE);

require_once 'templates/top.php';

if ($loggedin) {
	switch ($action) {
		default:
			require_once 'templates/index.php';
		break;
		case "list":
		case "delete":
		case "import":
			require_once 'templates/list.php';
		break;
		case "add":
			require_once 'templates/add.php';
		break;
		case "edit":
			require_once 'templates/edit.php';
		break;
		case "preview":
			require_once 'templates/preview.php';
		break;
		case "logout":
			logout();
		break;
	}
}
else {
	switch($action) {
		default:
			require_once 'templates/login.php';	
		break;
		case "login":
			/*
			*	Login submitted check for valid credentials
			*/
			$max_login_attempts = select_row("value", "`s_site-settings`", "slug='max-login-attempts'", "");
			$loggedin = check_login($max_login_attempts);
			$_SESSION['loggedin'] = $loggedin;
			if ($loggedin) {
				header("Refresh:0");
			}
			else {
				require_once 'templates/login.php';
			}
		break;
	}
}
?>